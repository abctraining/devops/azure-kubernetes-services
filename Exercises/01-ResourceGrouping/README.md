# Azure Kubernetes Services: The Essentials
# Hands-on Exercises #1

### Objective

In these exercises we will explore the methods of organizing resources in Kubernetes

### Parts

[Part 1: Pods](Part-01-Pods.md)

[Part 2: Namespaces](Part-02-Namespaces.md)

[Part 3: Labels](Part-03-Labels.md)

Return to the course [Table of Content](../README.md)
