# Exercise #1 : Part 1
## Pods

---

### Intro

Pods are the atomic unit of work for Kubernetes.  A pod is made up of one or more containers plus additional resources to provide a single logical unit that when created represents a single running instance of an application or Microservice. All work within Kubernetes runs within Pods.  In this section we will pods in Kubernetes.

---

### Pods

Usually within Kubernetes we prefer to take advantage of the declarative functionality that Kubernetes provides through higher level abstracts that controllers and operators provide.  But as we are learning we will first create create pods directly with add-hoc commands.

_Create our First pod_

```bash
kubectl run nginx --image=nginx:alpine --port=80
```

Here we tell Kubernetes to run a single pod named "nginx" using the "nginx:alpine" image.  That image is expected to have a service listening on TCP port 80 of the pod. (note this does not expose that service outside the cluster)

If you want to see other options you when running ad-hoc pods with 'kubectl' run:

_Display online help for kubectl run_

```bash
kubectl run --help
```

To list the current running pods run.

```bash
kubectl get pods
```

As we learned before we also could add the '-o wide' to get more columns.

```bash
kubectl get pods -o wide
```

Or we could 'describe' the pod to get all the information about the pod.  Our pod name is 'nginx'.

```bash
kubectl describe pod nginx
```

See if you can create three additional pods based of of these docker images all publishing port 80 on their respective ClusterIP addresses:

apache:
- httpd:latest

nginx:
- nginx:latest

caddy:
- caddy:latest

When you are completed you should see 4 different pods running in your environment.

You can now delete those pods with:

```bash
kubectl delete pod {pod_NAME}
```

Go ahead and delete each pod you created.  Then check to make sure all the pods have been deleted.

---

This concludes part 1 of Hands-on Exercise #1.  Continue on to Part 2 next.

[Part 2: Namespaces](Part-02-Namespaces.md)
