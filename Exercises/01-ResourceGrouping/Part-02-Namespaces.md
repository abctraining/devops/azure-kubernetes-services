# Exercise #1 : Part 2
## Namespaces

---

### Intro

Kubernetes provides a single cluster that we deploy our workloads to.  Inside that cluster we need the ability to sub-divide the cluster into different logical units to provide boundaries between between environments, users, applications, etc.  Kubernetes provides 'Namespaces' to organize and sub-divide different workloads.  In this exercise we will explore Kubernetes Namespaces.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/01-ResourceGrouping
~~~

### Namespaces

Create namespaces and run pods

Use these `kubectl` commands to create two new namespaces to run pods in.

_create two namespaces_

```bash
kubectl create -f dev-ns.yaml
kubectl create -f prod-ns.yaml
```

Now apply the two manifest files created in the new namespaces.

_create two pods with 2 namespaces_

```bash
kubectl create -f dev-pod.yaml -n dev
kubectl create -f prod-pod.yaml -n prod
```

Now you can use the `kubectl` get command to search for the pods in each namespace.

_Search for pods_

```bash
kubectl get pods
kubectl get pods -n dev
kubectl get pods -n prod

```

---

Restrict accessing prod pod from other namespace

```bash
kubectl create -f deny-from-other-ns.yaml
```

This blocks accessing `prod` pod from any other namespace with a NetworkPolicy.

_verify_
```bash
kubectl get pods -o wide -n dev nginx-dev
kubectl get pods -o wide -n prod nginx-prod
```

copy the IP of the two pods (nginx-dev and nginx-prod)

From nginx-prod

```bash
kubectl exec -it nginx-prod -n prod -- /bin/bash
```

Now with in the container running inside the pods attempt to ping the other pods.

_on the nginx-prod pod_
```bash
apt-get update
apt-get install -y iputils-ping
ping -c3 <nginx-dev-ip>
ping -c3 <nginx-prod-ip>
```
_both should work_
_use `exit` to leave the pod_


From nginx-dev

```bash
kubectl exec -it nginx-dev -n dev -- /bin/bash
```

_on the nginx-dev pod_
```bash
apt-get update
apt-get install -y iputils-ping
ping -c3 <nginx-dev-ip>
ping -c3 <nginx-prod-ip>
```
_ping to prod should not work_

_use `exit` to leave the pod_

## cleanup

```bash
kubectl delete ns dev
kubectl delete ns prod
```


---

This concludes part 2 of Hands-on Exercise #1.  Continue on to Part 3 next.

[Part 3: Labels](Part-03-Labels.md)
