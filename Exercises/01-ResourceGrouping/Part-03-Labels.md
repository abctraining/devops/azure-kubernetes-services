# Exercise #1 : Part 3
## Labels

---

### Intro

In addition to Kubernetes 'Namespaces' that provide organization of Kubernetes workloads there also is Kubernetes 'Labels'.  In Kubernetes 'Labels' are the primary way both internally for the Kubernetes objects and structurally for Kubernetes administrators tool to provide grouping and linking of resources.  In this exercise we will explore Kubernetes 'Labels'

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/01-ResourceGrouping
~~~


### labels

In the upcoming steps we will create a bunch of pods with various labels, so we can operate on these pods.

Let’s take a look at what we have created (_you may not have any if you cleaned up after other labs):


```bash
kubectl get po
```
You can get more details by showing and displaying the labels:

```bash
kubectl get po --show-labels
```

###### Create a list of Pods with labels

Use Sample Infrastructure to create a bunch of Pods (even though in real life we don't create pods)

```bash
kubectl create -f sample-infra-with-labels.yaml
```

This will creates a bunch of pods with labels
Wait for some time until all pods have been created

```bash
watch kubectl get pods
```

Use `CTRL+C` to exit the `watch` command.

---

###### Use various Selectors for pods

Now we use various selectors to search in labels to pull out pods, and delete them

```bash
kubectl get pods --selector env=production --show-labels
```

or

```bash
kubectl get pods -l env=production --show-labels
```

We can use and search multiple labels , this represents and

```bash
kubectl get pods -l env=production,dev-lead=amy --show-labels
```

We can also have the not equal to operator

```bash
kubectl get pods -l env=production,dev-lead!=amy --show-labels
```

Get the pods, which are in a list of versions - we use the in operator

```bash
kubectl get pods -l 'release-version in (1.0,2.0)' --show-labels
```

Get the pods, which are in a list of versions - we use the notin operator

```bash
kubectl get pods -l 'release-version notin (1.0,2.0)' --show-labels
```

###### add a label on an existing pod and delete the label

Changing labels of an existing pod

_Add a new label_

```bash
kubectl label po/cart-dev app=helloworldapp
kubectl get pods/cart-dev --show-labels
```

_Overwrite an existing label_

```bash
kubectl label po/cart-dev app=helloworldapp2 --overwrite
```

Note - if you do not specify "overwrite" option, it will complain that the KEY already has a VALUE

delete a label to an existing pod

_remove the label_

```bash
kubectl label po/cart-dev app-
```

Delete pods, which match some of the given pods [delete, get with a specific label, works for deployment, replication set too]

```bash
kubectl delete pods -l dev-lead=karthik
```

delete all pods which belong to the environment production

```bash
kubectl delete pods -l env=production
```

delete all pods in a namespace (we will see namespace in detail later. But right now, we have to delete all pods for cleanup, so we are using delete by namespace)

```bash
kubectl delete --all pods --namespace=default
```



---

This concludes part 3 of Hands-on Exercise #1.

Return to the [Exercise Page](../README.md)
