# Azure Kubernetes Services: The Essentials
# Hands-on Exercise #6

### Objective

In these exercises we will explore some options for application control within Kubernetes.

### Parts

[Part 1: Discovery](Part-01-Discovery.md)

[Part 2: Jobs](Part-02-Jobs.md)

[Part 3: Logging](Part-03-Logging.md)

Return to the course [Table of Content](../README.md)
