# Exercise #6 : Part 3
## Logging

---

### Intro


---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/06-AppControl
~~~

### Logs

Run a pod and check logs

```bash
kubectl create -f basic-logging.yaml
```

Check logs

```bash
kubectl logs pod/counter
```

Follow the logs from the pod

```bash
kubectl logs -f pod/counter
```

'CTRL+C' to exit

Destroy all resources

```bash
kubectl delete -f basic-logging.yaml
```


---

This concludes part 3 of Hands-on Exercise #6.

Return to the [Exercise Page](../README.md)
