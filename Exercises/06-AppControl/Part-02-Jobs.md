# Exercise #6 : Part 2
##

---

### Intro


---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/06-AppControl
~~~

### Jobs

In this tutorial we will learn how to create Jobs, CronJobs and inspect the containers and logs.

###### Create a simple job

```bash
kubectl create -f simplejob.yaml
```

Check for jobs, pods running

```bash
kubectl get jobs,po,svc
```

Check logs and describe the pods

```bash
kubectl describe job countdown

kubectl describe pod <countdown-pod_name>

kubectl logs <countdown-pod_name>
```

---

###### Create a Cron Job

```bash
kubectl create -f cronjob.yaml
```

Check for the running cronjob,pods,svc

```bash
kubectl get po,cronjob
```

Check if the pods are running and the age of the cronjob periodically (the LAST SCHEDULE should get reset every minute)
To get all pods that may have been completed use this

```bash
watch kubectl get po,cronjob
```

_When finished use `CTRL+C` to exit the `watch` command._

Do a describe on the pods and check the logs

```bash
kubectl describe cronjob periodiccron

kubectl logs <pod_name>
```

Edit the cronjob - Disable it

```bash
kubectl edit cronjob periodiccron
```

This will bring the job manifest up in `vim` editor. Change the flag to suspend to true, save and exit

Verify that the job is suspended.

```bash
kubectl get cronjobs -o wide
```

The suspended column should show as true, and the cron job should stop executing and stop creating new pods


---

This concludes part 2 of Hands-on Exercise #6.  Continue on to Part 3 next.

[Part 3: Logging](Part-03-Logging.md)
