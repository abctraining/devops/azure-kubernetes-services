# Azure Kubernetes Services: The Essentials
# Hands-on Exercise #4

### Objective

In these exercises we will explore additional options that we have to extend the PodSpec with.

### Parts

[Part 1: ConfigMaps](Part-01-ConfigMaps.md)

[Part 2: Secrets](Part-02-Secrets.md)

[Part 3: Volumes](Part-03-Volumes.md)

[Part 4: ResourceLimits](Part-04-ResourceLimits.md)

[Part 5: InitContainers](Part-05-InitContainers.md)

Return to the course [Table of Content](../README.md)
