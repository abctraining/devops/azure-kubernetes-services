# Exercise #4 : Part 4
## Resource Limits

---

### Intro

Compute resources even within a cluster are not infinite.  It is good to be able to reserve or limit resource usage within a cluster.  Here we will see "requests" (reservation) and "limits" (hard limits) can be applied to containers running in pods.  The scheduler will take these values into account when scheduling a pod to a node.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/04-PodSpec
~~~

### Pod Resource Limits

We will us the pod that was used in the volumes exercise extending it to add recource limits.  Following is the manifest file for the new 'limited-resources' pod.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: limited-resources
  labels:
    app: limited-resources
spec:
  containers:
  - name: ubuntu
    image: project42/s6-ubuntu:20.04
    volumeMounts:
    - mountPath: /mnt/scratch
      name: scratch
    resources:
      requests:
        memory: "64Mi"
        cpu: "50m"
      limits:
        memory: "128Mi"
        cpu: "50m"
  - name: centos
    image: project42/s6-centos:7
    volumeMounts:
    - mountPath: /mnt/scratch
      name: scratch
    resources:
      requests:
        memory: "64Mi"
        cpu: "50m"
      limits:
        memory: "128Mi"
        cpu: "100m"
  volumes:
  - name: scratch
    emptyDir: {}
```

We can see that the above spec is in the 'resources-pod.yaml' file.

```bash
cat resources-pod.yaml
```

Go ahead and create that pod in your Kubernetes cluster using the `kubectl` client.  Once running you should be able to list the pod with the following command.

```bash
kubectl get pod limited-resources
```

Now 'describe' the pod and notice the limits enforced on each container in the pod.

When resource limits are applied to pods Kubernetes also applies it to assist in calculating resource assignment on the cluster and within each node.   We can see that in the "Allocated resources" section of describing the nodes.  Also notice the resources in the "Non-terminated Pods" section.

```bash
kubectl describe nodes
```

When finished clean up the lab and delete the 'limited-resources' pod.

---

This concludes part 4 of Hands-on Exercise #4.  Continue on to Part 5 next.

[Part 5: InitContainers](Part-05-InitContainers.md)
