environment# Exercise #4 : Part 1
## ConfigMaps

---

### Intro

With Microservices it is important to be able to store environment variables separate from the application code.   ConfigMaps provide that functionality to Kubernetes Namespaces.  A configMap is scoped to a namespace and available to be exposed to processes in pods as environment variables, command arguments, file mounts, or even multiple files.  Here we will explore ConfigMaps.

---

Before we begin make sure all updates to the lab exercise repo are pulled locally.

```bash
cd ~/content/azure-kubernetes-services/
git pull
```

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/04-PodSpec
~~~

### ConfigMaps

Create a deployment which does not refer to config map ( has log level hard coded as error)

```bash
kubectl create -f reader-deployment.yaml
```

The content of that file looks like this.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: logreader
  labels:
    name: logreader
spec:
  replicas: 1
  selector:
    matchLabels:
      name: logreader
  template:
    metadata:
      labels:
        name: logreader
    spec:
      containers:
      - name: logreader
        image: karthequian/reader:latest
        env:
        - name: log_level
          value: "error"
```

Check for the running pods and describe the pod, and check logs for the running pod

```bash
kubectl get deploy,rs,po -o wide -l name=logreader
```

You should see the pod running.  Use that pod name in the following commands.

```bash
kubectl describe pod <pod_name>
```

Notice the "log_level" evironment variable in the "Containers" section.

```bash
kubectl logs <pod_name>
```

You should see that the "Log level" is "error" which is the value statically set through an environment variable in the manifest file.

Next we will improve this by taking advantage of ConfigMaps in Kubernetes to define the "Log Level" externally in one location that can be shared by multiple pods in the same environment.

---

Before creating the deployment from a manifest file let us define a ConfigMap named 'logger' with key/value pair for the "log_level".

```bash
kubectl create configmap logger --from-literal=log_level=debug
```

Check if the Config Map was created

```bash
kubectl get configmap logger -o wide
```

You can also descibe the new configMap.  Notice the key/value in the description.

```bash
kubectl describe configmap logger
```

Now create the Deployment from the following YAML manifest file found in 'reader-configmap-deployment.yaml'

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: logreader-dynamic
  labels:
    name: logreader-dynamic
spec:
  replicas: 1
  selector:
    matchLabels:
      name: logreader-dynamic
  template:
    metadata:
      labels:
        name: logreader-dynamic
    spec:
      containers:
      - name: logreader
        image: karthequian/reader:latest
        env:
        - name: log_level
          valueFrom:
            configMapKeyRef:
              name: logger               #Read from a configmap called log-level
              key: log_level             #Read the key called log_level
```

Go ahead and create the deployment

```bash
kubectl create -f reader-configmap-deployment.yaml
```

Check for the running pods and describe the pod, and check logs for the running pod

```bash
kubectl get deploy,rs,po -o wide -l name=logreader-dynamic
```

As before describe the pod.  This time notice that the environment variable is from the ConfigMap.

```bash
kubectl describe pod <pod_name>
```

Check the logs again.

```bash
kubectl logs <pod_name>
```

This time it is set to "debug".


Go ahead and delete the 'logger' configMap and both deplopments to clean up after the exercise.

---

This concludes part 1 of Hands-on Exercise #4.  Continue on to Part 2 next.

[Part 2: Secrets](Part-02-Secrets.md)
