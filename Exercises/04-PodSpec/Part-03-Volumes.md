# Exercise #4 : Part 3
## Volumes

---

### Intro

Volumes are an addition resource that can be attached to pods.  There are two major forms of storage volumes within Kubernetes; Volumes and PersistentVolumes.  In this section we will focus on the simpler form, the Volume.  This type of storage is useful to share data between containers running inside a pod.  It is best to treat Kubernetes Volumes as ephemeral storage.  If you need longterm storage you should look at PersistentVolumes which we will cover later.  In this exercise we will explore the Kubernetes Volume.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/04-PodSpec
~~~

### Volumes

We define the volume within the PodSpec.  Here is the manifest file we will be using in this exercise.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: shared-volume
  labels:
    app: shared-volume
spec:
  containers:
  - name: ubuntu
    image: project42/s6-ubuntu:20.04
    volumeMounts:
    - mountPath: /mnt/scratch
      name: scratch
  - name: centos
    image: project42/s6-centos:7
    volumeMounts:
    - mountPath: /mnt/scratch
      name: scratch
  volumes:
  - name: scratch
    emptyDir: {}
```

Notice that in the PodSpec there is the volume defined as an "emptyDir:" type volume.  That results in a simple volume that does not persist outside of life of that specific pod.  You also will see that the volume is mounted into both containers of the pod (ubuntu, centos) in the "/mnt/scratch" path.  Let's create that resource with 'kubectl create'

```bash
kubectl create -f volume-pod.yaml
```

We cat "watch" the pod start up by taking advantage of the 'watch' command.  This command will refresh the 'kubectl get pods -l app=shared-volume' command every 2 seconds, giving us a constantly updating view of the resources in the Kubernetes cluster.

```bash
watch kubectl get pods -l app=shared-volume
```

Notice that the READY column when 'Running' shows '2/2' that indicates that there are 2 running containers in that pod. The 'READY' column shows {RunningPods}/{DesiredPods}.

When you are done with the 'watch' command use `ctrl+c` to exit.

Let us explore the new volume with the 'kubectl describe' command.

```bash
kubectl describe pod shared-volume
```

Observe the volume section in the pod, listed below.  Also in the description see if you can find where the 'scratch' volume is mounted in each container.

```yaml
...
Volumes:
  scratch:
    Type:       EmptyDir (a temporary directory that shares a pod's lifetime)
    Medium:
    SizeLimit:  <unset>
...
```

We can connect to the pod by executing a bash shell in the 'ubuntu' container of the pod.  The '-it' option tells Kubernetes that you want to "interact" (i) with the process and the process needs a "tty" (t) interface to connect to which is required to run a shell like bash.  The '-c' option tells Kubernetes which "container" you want to connect to.  The '--' is used to indicate that "standard-in" and "standard-out" are passed between 'kubectl' and the '/bin/bash' process.

```bash
kubectl exec -it shared-volume -c ubuntu -- /bin/bash
```

The above command results in presenting you with a shell inside the container running in the pod.  From there you can execute standard commands that are available within that container.  The availible commands are binaries from the container image or installed into the container after the container starts. We can see that the container is based from an Ubuntu image.

```bash
cat /etc/os-release
```

We can use the mount command to see that the "scratch" volume is mounted in the container.

```bash
mount | grep scratch
```

Now let us put a file into that "scratch" volume.  We will just copy the "os-release" file to it and append ".ubuntu" to the new filename.  We also can list the files in "/mnt/scratch" to see the new 'os-release.ubuntu' file.

```bash
cp /etc/os-release /mnt/scratch/os-release.ubuntu
ls /mnt/scratch/
```

Exit the bash shell connected to the Ubuntu container in the pod.

```bash
exit
```

Now we can connect to the other container in the same pod, the "centos" container.  We will explore that container the same way we did the "ubuntu" container.

```bash
kubectl exec -it shared-volume -c centos -- /bin/bash
```

Notice the distribution and release are different.

```bash
cat /etc/os-release
```

The same volume is mounted in the same path also on the "centos" container.

```bash
mount | grep scratch
```

Let us copy and additional file to the shared volume.

```bash
cp /etc/os-release /mnt/scratch/os-release.centos
ls /mnt/scratch/
```

Charnge to the '/mnt/scratch' directory using the 'cd' command.  Once there check out the content of both the files in that shared volume between containers in the pod using the 'cat' command.  Once finished exploring exit the bash shell connected to the Ubuntu container in the pod.

```bash
exit
```

You can remove the 'shared-volume' pod using the same YAML file we used to create it.

```bash
kubectl delete -f volume-pod.yaml
```

---

This concludes part 3 of Hands-on Exercise #4.  Continue on to Part 4 next.

[Part 4: Resource Limits](Part-04-ResourceLimits.md)
