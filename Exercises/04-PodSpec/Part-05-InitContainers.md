# Exercise #4 : Part 5
## InitContainers

---

### Intro

Processes with in pod generally run indefinitely and do not stop until they receive a SIGTERM signal to shutdown.  There is a desire to have tasks that can run inside a pod as admin tasks that can come to completion.  That functionality is handled through the use of InitContainers.  In this section we will look at the InitContainer.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/04-PodSpec
~~~

### InitContainers

Again we will build on the PodSpec from the previous exercise.  This time adding an initContainer to do some work before the main containers start.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: init-git-clone
  labels:
    app: init-container
spec:
  containers:
  - name: ubuntu
    image: project42/s6-ubuntu:20.04
    volumeMounts:
    - mountPath: /mnt/scratch
      name: scratch
    resources:
      requests:
        memory: "64Mi"
        cpu: "50m"
      limits:
        memory: "128Mi"
        cpu: "50m"
  - name: centos
    image: project42/s6-centos:7
    volumeMounts:
    - mountPath: /mnt/scratch
      name: scratch
    resources:
      requests:
        memory: "64Mi"
        cpu: "50m"
      limits:
        memory: "128Mi"
        cpu: "100m"
  initContainers:
  - name: git-clone
    image: alpine/git
    args:
      - clone
      - https://gitlab.com/abctraining/devops/azure-kubernetes-services.git
    volumeMounts:
    - mountPath: /git
      name: scratch
    resources:
      limits:
        memory: "128Mi"
        cpu: "500m"
  volumes:
  - name: scratch
    emptyDir: {}
```

Again that yaml file exists so go ahead and create that pod resource in Kuberenetes.

```bash
kubectl create -f initcontainer-pod.yaml
```

After the new 'init-git-clone' pod starts up go ahead and 'describe' it.  Notice in the "Init Containers" section there is a 'git-clone' container that has "Terminated" because it has "Completed" with an exit code of "0".  We can even see the logs that where generated when that init container ran.

```bash
kubectl logs init-git-clone -c git-clone
```

Next connect to a '/bin/bash' shell in either the 'ubuntu' or the 'centos' container in the 'init-git-clone' pod. You will use 'kube exec' to make that connection, the '-c' option indicates what container to connect to inside the pod.  Once on a shell inside the container on the pod navigate to the '/mnt/scratch' directory using 'cd' and list the content.  If everything is successful you should find the git repo for these exercise in that shared volume.

When finished exploring 'exit' the shell in the container running in the pod and remove the pod from your cluster.

---

This concludes part 5 of Hands-on Exercise #4.

Return to the [Exercise Pages](../README.md)
