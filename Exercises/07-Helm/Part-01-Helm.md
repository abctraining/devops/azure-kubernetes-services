# Exercise #7 : Part 1
## Helm

---

### Intro


---

### Helm

Helm is a package manager for Kubernetes that allows developers and operators to more easily configure and deploy applications on Kubernetes clusters.

We will be using Helm v3 which is self contained in a single `helm` binary,  no need for a `tiller` service in Kubernetes as was needed in older versions of Helm.

---

_Add a package repo for helm_
```bash
curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
sudo apt-add-repository "deb https://baltocdn.com/helm/stable/debian/ all main"
```

_Install the helm package_
```bash
sudo apt install -y helm
```

_verify the install_
```bash
helm version
```

---


#### Example Helm Commands

_search charts on artifact hub_
```bash
helm search hub
```

_list local repos_
```bash
helm repo list
```

_add the "stable" repo_
```bash
helm repo add stable https://charts.helm.sh/stable
```

_add k8s@home repo_
```bash
helm repo add k8s-at-home https://k8s-at-home.com/charts/
```

_update the local repos_
```bash
helm repo update
```

_search for "unifi" charts in local repos_
```bash
helm search repo unifi
```

_gather various info from the "stable/unifi" chart_
```bash
helm show chart stable/unifi

helm show readme stable/unifi

helm show chart k8s-at-home/unifi

helm show readme k8s-at-home/unifi

helm show values k8s-at-home/unifi
```

_install a chart as a release_
```bash
helm install <releaseName> [options] <repo>/<chart>
```

_uninstall a release_
```bash
helm uninstall <releaseName>
```
---

#### Install Wordpress using helm

Make sure you have a 'Loadbalancer' enabled on your cluster to complete this version of the instalation of Wordpress via helm chart.

_Add the bitnami repo_
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
```

_search for wordpress charts_
```bash
helm search repo wordpress
```
_gather info_
```bash
helm show chart bitnami/wordpress

helm show readme bitnami/wordpress

helm show values bitnami/wordpress
```

_install a wordpress release_
```bash
helm install wordpress-release01 --set wordpressUsername=admin --set wordpressPassword=password --set mariadb.mariadbRootPassword=secretpassword --set volumePermissions.enabled=true --set mariadb.volumePermissions.enabled=true bitnami/wordpress
```

_install a second release without PVs_
```bash
helm install wordpress-release02 --set wordpressUsername=admin --set wordpressPassword=password --set mariadb.mariadbRootPassword=secretpassword --set persistence.enabled=false --set mariadb.primary.persistence.enabled=false bitnami/wordpress
```

__Note the output__

_get current values for the wordpress-release01 release_
```bash
helm get values wordpress-release01
```

_using "kubectl" gather info_
```bash
kubectl get svc -l app.kubernetes.io/instance=wordpress-release01

kubectl get deploy -l app.kubernetes.io/instance=wordpress-release01

kubectl get pods -l app.kubernetes.io/instance=wordpress-release01
```

* Try connecting to the service using the NodePort for the Service
* Notice that there are both HTTP and HTTPS ports

_uninstall the wordpress release_
```bash
helm uninstall --keep-history wordpress-release01
helm uninstall --keep-history wordpress-release02
```

_remove the PV and Secrets_
```bash
kubectl delete persistentvolumeclaim/data-wordpress-release01-mariadb-0
kubectl delete secret/sh.helm.release.v1.wordpress-release01.v1
kubectl delete secret/sh.helm.release.v1.wordpress-release02.v1
```


---

This concludes part 1 of Hands-on Exercise #7.

Return to the [Exercise Page](../README.md)
