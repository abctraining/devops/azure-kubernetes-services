# Azure Kubernetes Services: The Essentials
# Hands-on Exercise #7

### Objective

In these exercises we will look at using Helm as a package manager for Kubernetes.

### Parts

[Part 1: Helm](Part-01-Helm.md)

Return to the course [Table of Content](../README.md)
