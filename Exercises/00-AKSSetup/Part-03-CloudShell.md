# Exercise #0 : Part 3
## Cloud Shell

---

### Intro

The main client to provide access to the management plane of Kubernetes through the Kubernetes API is the CLI client 'kubectl'.  The 'kubectl' client can run on any machine that has access to the Kubernetes API.  It is available on all major platforms.  Within this workshop we will be utilizing an online shell provided by Azure know as 'Cloud Shell'.  In this exercise we will initialize the 'Cloud Shell' setting up storage and explore a few common commands.

---

At the conclusion of the last part of this exercise we where presented with a "Your deployment is complete" message.  Now we would like to connect to a shell that allows us to then connect to the newly created cluster.  We will use the Azure "Cloud Shell" for that purpose.

### Cloud Shell

Select the "Connect to cluster" from your AKS deployment page.

![Cloud Shell - Connect to Cluster](images/00-03-01.png "Cloud Shell - Connect to cluster")

You will be presented with options that you have to connect to your cluster.  We will be using the "Cloud Shell".  When opening the Cloud Shell from this page Azure will run the "az account set" and the "az aks get-credentials" commands listed here for us.  But if you connect a differet way you may have to run those commands before using "kubectl" to connect to your Kubernetes Cluster.

Select the "Open Cloud Shell" link on this page.

![Cloud Shell - Open Cloud Shell](images/00-03-02.png "Cloud Shell - Open Cloud Shell")

Cloud Shell has two options "Bash" or "PowerShell".  We will be using "Bash" so Select the "Bash" link.

![Cloud Shell - Bash](images/00-03-03.png "Cloud Shell - Bash")

The first time you start your "Cloud Shell" you will need to create a storage share to provide a location to store your shell files.  Click on the "Create storage" button.

![Cloud Shell - Create storage](images/00-03-04.png "Cloud Shell - Create storage")

After a short time you will be presented with a shell.  Notice that the two require "az" commands have already been executed for this session.

![Cloud Shell - Connected](images/00-03-05.png "Cloud Shell - Connected")

---

The azure shell we are using is running on a container that has the "az" configuration setup for our account.  _Note: If you want to run this cloud shell locally as a container you can by using the 'mcr.microsoft.com/azure-cloudshell:latest' image.  More info can be found [here](https://github.com/Azure/CloudShell)._

Verify that the 'az' is tied to the azure account.

```bash
az account list
```

![Cloud Shell - az account](images/00-03-06.png "Cloud Shell - az account")

---

### Copy Lab files to the Cloud Shell

Let us create a location to store the hands-on exercise source files.

~~~shell
mkdir -p ~/content
~~~

We can change our  working directory to this newly created location by changing our directory to it.

~~~shell
cd ~/content
~~~

In this new folder we are going to use the `git` client to pull the lab content locally by cloning the repo using the https protocal.

~~~shell
git clone https://gitlab.com/abctraining/devops/azure-kubernetes-services.git
cd azure-kubernetes-services
~~~

### A scavenger hunt

Now it's time for a scavenger hunt.  The `src` files are now located on your lab instances file system.  See if you can locate the file associated with Hands-on Exercise 00-AKSSetup - Cloud Shell.  Once you locate that file you can view the content by concatenating it with the `cat` command.

### Conclusion

You now have the access and tools needed to complete upcoming hands-on exercises.  Over the next days we will get quite a bit of hands on experience with Kubernetes.

---

This concludes part 3 of Hands-on Exercise #0.  Continue on to Part 4 next.

[Part 4: kubectl](Part-04-kubectl.md)
