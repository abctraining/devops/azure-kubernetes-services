# Exercise #0 : Part 4
## kubectl

---

### Intro

The 'kubectl' cli client is the standard client that is used when managing Kubernetes clusters.  It connects to the unified API endpoint within the control plane of a Kubernetes cluster.  It is a "Unified" API client, all communications for all Kubernetes resources within Kubernetes flow through the Kubernetes API.  

---

### kubectl


_Check the K8s API Resources_

```bash
kubectl api-resources
```

The 'kubectl api-resources' command returns all available API resources on the Kubernetes cluster you are connecting to.  All Kubernetes cluster have the core K8s objects availible in the API.  You can recognize these resources by the "APIVERSION" for each resource.  For instance,  'ApiVersion v1' are objects there where released with Kubernetes version 1.0.  'ApiVersion apps/v1' are additional objects that where added to Kubernetes at a later time.  Other examples of core APIVERSIONs are stuff like 'autoscaling/v2', 'batch/v1, and 'policy/v1' to name a few.  Depending on your cluster you will probably also see listed additional resources that extend the Kubernetes Unified API with additional objects bringing additional functionality to that specific Kubernetes cluster.

Let us explore the nodes within our Kubernetes cluster using the 'Node' API object via 'kubectl'

_List the K8s Nodes in the cluster_

```bash
kubectl get nodes
```

This command returns a list of "Nodes" in the Cluster.  Every Kubernetes cluster is made up of one or more Nodes.  We can find the "NAME", "STATUS", "ROLES", "AGE" and the K8s "VERSION" of each node.  The 'kubectl' will return the most common information for that specific API object.  If you would like to get additional information about a specific node 'kubectl' has an '-o' option to adjust the "output" columns for that specific request, for instance run this command an notice the additional columns listed for the nodes in the cluster.

_Wide listing of Nodes in the cluster_

```bash
kubectl get nodes -o wide
```
Notice that this time 'kubectl' include some additional columns like "INTERNAL-IP", "EXTERNAL_IP", and "KERNEL-VERSION".  Each Kubernetes API resource has different but appropriate columns that are returned with 'kubectl get {Resource}'  and 'ubectl get {Resource} -o wide'.  If you need even more information about any specific Kubernetes resource you can "describe" that resource with 'kubectl'.

_Describe a specific K8s Node_

```bash
kubectl describe node {Node_NAME}
```

Don't forget to replace "{Node_NAME}" with one of the nodes that you listed with the 'kubectl get nodes' command.

Notice that this time 'kubectl' returned much more information about that specific node.  You can do the same with most Kubernetes resources.

---

This concludes part 4 of Hands-on Exercise #0.

Return to the [Exercise Page](../README.md)
