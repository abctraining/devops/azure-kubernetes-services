# Exercise #0 : Part 1
## Connect To Azure

---

### Intro

The initial step of any lab exercises is getting access to the lab environment.  These labs will be hosted in Azure.  The instructor will provide all the information required to connect to the Azure account provided within this workshop.  Of course Azure being a public cloud there is nothing preventing you from working through each of these labs on your own Azure account.

---

### Connect to Azure

You will need to connect to Azure using the credentials shared to you for the Lab environment.  If you are currently logged into a different azure account be sure to select "Use anouther account".


[https://portal.azure.com](https://portal.azure.com)

![Azure Login - Username](images/00-01-01.png "Azure Login - Username")

![Azure Login - Password](images/00-01-02.png "Azure Login - Password")

On the first login you will need to update the password.

![Azure Login - Change Password](images/00-01-03.png "Azure Login - Change Password")

If you receive a request to "Help us protect your account" for now just select the "Skip for now" option.  These will be short lived accounts.

![Azure Login - Protect Account](images/00-01-04.png "Azure Login - Protect Account")

This should present you with the Azure Dashboard.  If you are not familiar with the dashboard take the tour to get familiar with the interface.

---

This concludes part 1 of Hands-on Exercise #0.  Continue on to Part 2 next.

[Part 2: AKS](Part-02-AKS.md)
