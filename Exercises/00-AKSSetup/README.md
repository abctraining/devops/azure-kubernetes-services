# Azure Kubernetes Services: The Essentials
# Hands-on Exercises #0

### Objective

In these exercises we will connect and initialize the lab environment

### Parts

[Part 1: Connect to Azure](Part-01-ConnectToAzure.md)

[Part 2: AKS](Part-02-AKS.md)

[Part 3: Cloud Shell](Part-03-CloudShell.md)

[Part 4: kubectl](Part-04-kubectl.md)

Return to the course [Table of Content](../README.md)
