# Exercise #0 : Part 2
## AKS

---

### Intro

Kubernetes can run on many different platforms.  Public cloud providers like Azure provide Kubernetes as a service to abstract the Kubernetes cluster from the user simplifying the management.  Azure's Kubernetes as a service platform is called Azure Kubernetes Services (AKS).  In this workshop we will use AKS as our Kubernetes cluster.  So here we will provision a AKS cluster.

---

### Azure Kubernetes Services

Within the Azure dashboard you can always get to the Kubernetes services page by searching for "Kubernetes services" in the search bar.

![Kubernetes services - Search](images/00-02-01.png "Kubernetes services - Search")

You will now be presented with the Kubernetes service Dashboard.  This dashboard will display all the Kubernetes clusters within this Azure subscriptions. We will each be adding to this list as we create our own Kubernetes cluster with AKS.

Let us create our Cluster.

- In the "Project details" section:
  - On the "Create" button Select "Create a Kubernetes cluster"
  - Using the "ABC Default" Subscription select the "ABCLabs_EastUS" resource Resource group
  - Select the "Dev/Test ($)" "Cluster preset configuration"
  - Set the "Kubernetes cluster name" to "lab##-cluster" replacing the "##" with the number corresponding to your username
  - Leave the "Region" set to "(US) East US"
  - Leave the "Availability zones" set to "None"
  - The "Kubernetes version" can stay as the "(default)"
  - Leave the "API server availability" set to "99.5%"

![Kubernetes services - Basic Part 1](images/00-02-02.png "Kubernetes services - Basic 1")

- In the "Primary node pool" section:
  - for the "Node size" select the "Change size" link
    - On the new page Select the "B2s" size
    - Then click on the "Select" button to return to the "Primary node pool" section
  - Set the "Scale Method" to "Manual"
  - Set the "Node count" to "2"

![Kubernetes services - Basic 2](images/00-02-03.png "Kubernetes services - Basic 2")

- Once you have all the setting correct on this page select "Next Node pools"
- We will leave the a number of pages as the defaults, the next changes are on the "Networking" page
  - Click "Next : Access"
  - Click "Next : Networking"
    - Leave the "Network configuration" set to "Kubelet"
    - On the Networking section verify that the "DNS name prefix" is set to "lab##-cluster-dns" where ""##"" is your lab number
    - For the "Network policy" select "Calico" so we can later utilize standard Kubernetes NetworkPolicies
  - Click "Next : Integrations"
  - Click "Next : Advanced"
  - Click "Next : Tags"
  - Click "Next : Review + create"
    - If everything is correct you should receive a "Validation passed"
  - Select the "Create" button to create your cluster

Here a the rest of the screenshots that you should expect.  

###### Node Pools:

![Kubernetes services - Node Pools](images/00-02-04.png "Kubernetes services - Node Pools")

###### Access

![Kubernetes services - Access](images/00-02-05.png "Kubernetes services - Access")

###### Networking

![Kubernetes services - Networking](images/00-02-06.png "Kubernetes services - Networking")

###### Integrations

![Kubernetes services - Integrations](images/00-02-07.png "Kubernetes services - Integrations")

###### Advanced

![Kubernetes services - Advanced](images/00-02-08.png "Kubernetes services - Advanced")

###### Tags

![Kubernetes services - Tags](images/00-02-09.png "Kubernetes services - Tags")

###### Review + create

![Kubernetes services - Review create](images/00-02-10.png "Kubernetes services - Review create")

---

It will take a few minutes for Azure to provision your Kubernetes Cluster as it is spinning up new virtual machines and setting the new machines as your Kubernetes "Worker Nodes" for your cluster.  Eventually you should recieve a "Your deployment is complete" message.  You now have a Kubernetes cluster running on AKS.

![Kubernetes services - Complete](images/00-02-11.png "Kubernetes services - Complete")

---

This concludes part 2 of Hands-on Exercise #0.  Continue on to Part 3 next.

[Part 3: Cloud Shell](Part-03-CloudShell.md)
