# Exercise #5 : Part 2
## DaemonSets

---

### Intro

We have seen how a ReplicaSet is used to replicate a set of pods within a Kubernetes cluster based on the replica value.  DaemonSets also create sets of pods within a cluster, this time though based on the number of Nodes.  Specifically one pod per node or one pod per a subset of nodes.  Here we explore the DaemonSet in Kubernetes. 

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/05-PodControllers02
~~~

### DaemonSet

We will create a daemonSet for this manifest file.

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  labels:
    app: nginx-ds
  name: example-daemonset
spec:
  selector:
    matchLabels:
      app: nginx-ds
  template:
     metadata:
       labels:
        app: nginx-ds
     spec:
       containers:
         - name: nginx
           image: nginx:1.9.1
```

Apply that manifest to the Kubernetes cluster.

```bash
kubectl apply -f daemonset-example.yaml
```

List the resources created with from that manifest.

```bash
kubectl get daemonset,pods -l app=nginx-ds
```

Notice that you have one "example-daemonset" pod on each node of the cluster.  DaemonSets can also be scheduled on a subset of the Nodes in the cluster by using a "nodeSelector" to require the pods to only be scheduled on the nodes with the correct labels.

See if you can see the difference with the updated manifest found in 'daemonset-example-v2.yaml'.

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  labels:
    app: nginx-ds
  name: example-daemonset
spec:
  selector:
    matchLabels:
      app: nginx-ds
  template:
     metadata:
       labels:
        app: nginx-ds
     spec:
       containers:
         - name: nginx
           image: nginx:1.9.1
       nodeSelector:
         approle: nginx
```

Let us update the DamonSet with this new manifest and see what happens.

```bash
kubectl apply -f daemonset-example-v2.yaml
```

Check the results

```bash
kubectl get daemonset,pods -l app=nginx-ds
```

Since we have yet to add the label to a node in the cluster we de not have any pods running within the DaemonSet.

We can see all the labels on the nodes with this command.

```bash
kubectl get nodes --show-labels
```

 Most of the labels are used internally for Kubernetes but as indicated we can also add our own labels.  Add the "approle=nginx" label to a node in your cluster.

```bash
kubectl label node {NodeName} approle=nginx
```

Once the label is added check the DaemonSet againg.

```bash
kubectl get daemonset,pods -l app=nginx-ds
```

Now try adding the label to the other node and see how the DaemonSet creates a new pod on that node.

If you want to delete a label you can by appending a "-" to the label key. You also need to use the "--overwrite" option to allow overwriting of an existing label.

```bash
kubectl label node {NodeName} approle- --overwrite
```

When finished delete the daemonSet and remove "approle" labels from any nodes.

---

This concludes part 2 of Hands-on Exercise #5.  Continue on to Part 3 next.

[Part 3: StatefulSets](Part-03-StatefulSets.md)
