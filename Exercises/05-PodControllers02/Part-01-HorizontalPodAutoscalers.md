# Exercise #5 : Part 1
## HorizontalPodAutoscalers

---

### Intro

So far we have seen scaling happening by simply adjusting the replica value in the deployment resource.  It would be nice to allow an internal to Kubernetes controller access to do the same.  That is where the 'HorizontalPodAutoscaler' (HPA) comes in.  It is a controller that we can give a min / max number of replicas to plus a CPU usage threshold to allow it to scale up and down as the demand for the resources of that pod set change.  The HorizontalPodAutoscaler does require the definition of resource limits within the podSpec and the exists of a "Metrics Server" within your cluster to have enough information to calculate the CPU utilization.  Let us explore the HPA in this exercise.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/05-PodControllers02
~~~

### HorizontalPodAutoscalers

First lets delete all prior pods, deployments and services

```bash
kubectl get deploy,pods,svc,rs,rc,hpa,jobs,cj -o wide

kubectl delete --all deploy,pods,svc,rs,rc,hpa,jobs,cj -n default
```

Create a deployment and a svc

```bash
kubectl create -f helloworld-dep-svc.yaml
```

Check the deployments and pods

```bash
kubectl get po,deploy,svc -o wide
```

---

### Scale on an existing deployment

Scale the deployment created in last step and see the effect on the number of pods, rs, and deployment

```bash
kubectl scale deploy helloworld-all-deployment --replicas=2
```

Check the scaling in "describe", at the bottom in the "Events" section, you will see that it's scaled from 1 to 2

```bash
kubectl describe deploy helloworld-all-deployment
```

---

### Use Auto-Scaling on an existing deployment

Add autoscale to an existing deployment helloworld-all-deployment

```bash
kubectl autoscale deployment helloworld-all-deployment --min=3 --max=10
```

check the HPA

```bash
watch kubectl get hpa,deploy,rs,pods
```

--- Wait for sometime, pods will get increased from 2 to 3 due to autoscaler.

_Use `CTRL+C` to exit the `watch` command._

##### Cleanup

```bash
kubectl delete --all deploy,svc,hpa -n default
```

---

### Functionality of Roll outs and Roll Backs

Create the deployment and service for the navbar

```bash
kubectl create -f navbar-black.yaml
```

```bash
kubectl get po,deploy,svc --show-labels -o wide
```

get the port for the LoadBalancer and check the URL for the node with the port

`http://<public_ip_address>:<Service_NodePort>`

Rollout a new deployment

```bash
kubectl set image deploy/header-deployment navbarworld=karthequian/helloworld:blue
```

Check the current status on pods , Deployment and replicasets (we will see a new replica set starting up, and now we will have 2 instances of the replica set)

```bash
kubectl get po,deploy,rs --show-labels -o wide
```

We can also do a describe on the replica set, and we shall see gradually all the pods moving to the other replica set

```bash
kubectl describe deploy header-deployment
```

Try to hit the front end again, and we should see the header change to blue from black

`http://<public_ip_address>:<Service_NodePort>`

Try the rollback the deployment now, and keep monitoring the replicasets , pods and deployment. You can use an additional terminal with the 'watch' command.

```bash
kubectl rollout undo deployment/header-deployment
```

Check the current status on pods , Deployment and ReplicaSets (we will see the pods going back to the old replica set)

```bash
watch kubectl get po,deploy,rs --show-labels -o wide
```

hit the url front end again to see the color of the header turn to black again

`http://<public_ip_address>:<Service_NodePort>`

p.s - We can also see the history of the rollout , this is enabled by the `--record` (DEPRECATED in recent versions of K8s), sometimes it may not display the history always as desired. Alternative is to use describe

```bash
kubectl rollout history deployment/header-deployment
```

Let's clean up

```bash
kubectl delete --all svc,deploy,pod,hpa --namespace=default
```


---

This concludes part 1 of Hands-on Exercise #5.  Continue on to Part 2 next.

[Part 2: DaemonSets](Part-02-DaemonSets.md)
