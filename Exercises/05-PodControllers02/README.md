# Azure Kubernetes Services: The Essentials
# Hands-on Exercise #5

### Objective

In these exercises we will look at additional Kubernetes objects to control the creation and management of Pods.

### Parts

[Part 1: HorizontalPodAutoscaler](Part-01-HorizontalPodAutoscalers.md)

[Part 2: DaemonSets](Part-02-DaemonSets.md)

[Part 3: StatefulSets](Part-03-StatefulSets.md)

Return to the course [Table of Content](../README.md)
