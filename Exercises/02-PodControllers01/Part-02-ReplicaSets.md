# Exercise #2 : Part 2
## ReplicaSets

---

### Intro

The first of two new object provided in the 'apps/v1' ApiVersion that ReplicaSets where decomposed into is the 'ReplicaSet'.  As the name indicates the ReplicaSet is used make sure there are the correct number of pods based on a specific PodSpec.  In this exercise we will exploe the ReplicaSet object.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/02-PodControllers01
~~~

### ReplicaSets

ReplicaSets are a sort of hybrid, in that they are in some ways more powerful than Replication Controllers,
and in others they are less powerful. Replica Sets are declared in essentially the same way as Replication Controllers,
except that they have more options for the selector. Use the file rs.yaml for this part

__The first rs.yaml file is similar to rc.yaml__

###### rs.yaml

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: soaktestrs
spec:
  replicas: 3
  selector:
    matchLabels:
      app: soaktestrs
  template:
    metadata:
      labels:
        app: soaktestrs
        environment: dev
    spec:
      containers:
      - name: soaktestrs
        image: nickchase/soaktest
        ports:
        - containerPort: 80
```

Go ahead and create the replica-set, describe and delete it.
You may chose to skip this part, as this replica-set is similar to replication-controller we checked earlier.


### Replica set with more options

###### rs_selector2.yaml

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: soaktestrs1
spec:
  replicas: 3
  selector:
   matchExpressions:
     - {key: app, operator: In, values: [soaktestrs, soaktestrs, soaktest]}
     - {key: tier, operator: NotIn, values: [production]}
  template:
    metadata:
      labels:
        app: soaktestrs
        tier: dev
    spec:
      containers:
      - name: soaktestrs
        image: nickchase/soaktest
        ports:
        - containerPort: 80
```

In this case, it’s more or less the same as when we were creating the Replication Controller, except we’re using matchLabels instead of label.  But we could just as easily have said (has matchExpressions):


Two different conditions defined in matchExpressions:
>  - The app label must be soaktestrs, soaktestrs or soaktest <br>
>  - The tier label (if it exists) must not be production

Let’s go ahead and create the Replica Set and get a look at it:

```bash
kubectl create -f rs_selector2.yaml
```

###### _output:_
```
replicaset.extensions/soaktestrs1 created
```
We can describe the Replica Sets

```bash
kubectl describe replicaset soaktestrs1
```

###### _output:_
```
Name:         soaktestrs1
Namespace:    default
Selector:     app in (soaktest,soaktestrs,soaktestrs),tier notin (production)
Labels:       app=soaktestrs
              environment=dev
Annotations:  <none>
Replicas:     3 current / 3 desired
Pods Status:  3 Running / 0 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=soaktestrs
           environment=dev
  Containers:
   soaktestrs:
    Image:        nickchase/soaktest
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From                   Message
  ----    ------            ----  ----                   -------
  Normal  SuccessfulCreate  27s   replicaset-controller  Created pod: soaktestrs1-7xmr5
  Normal  SuccessfulCreate  27s   replicaset-controller  Created pod: soaktestrs1-m2fqj
  Normal  SuccessfulCreate  27s   replicaset-controller  Created pod: soaktestrs1-rt68h  
```

We can get the pods

```bash
kubectl get pods
```

###### _output:_
```
NAME               READY     STATUS    RESTARTS   AGE
soaktestrs1-7xmr5   1/1     Running   0          74s
soaktestrs1-m2fqj   1/1     Running   0          74s
soaktestrs1-rt68h   1/1     Running   0          74s
```

As we can see, the output is pretty much the same as for a Replication Controller (except for the selector),
and for most intents and purposes, they are similar.  

##### Cleanup

```bash
kubectl delete replicaset soaktestrs1
```

__Explore bad options__ :- To see the errors when the labels and selectors does not match , feel free to try out the other replica set in the same location that has a mis-matched label:-

This should give an error

```bash
kubectl create -f rs_selector2_bad.yaml
```

###### _output:_
```
The ReplicaSet "soaktestrsbad" is invalid: spec.template.metadata.labels: Invalid value: map[string]string{"app":"soaktestrspq", "tier":"prod"}: `selector` does not match template `labels`
```

###### Fix:

Add the required label to the matchExpression like below:

```yaml
matchExpressions:
     - {key: app, operator: In, values: [soaktestrs, soaktestrs, soaktest, soaktestrspq]}
```

Create the resource again

##### Cleanup

```bash
kubectl delete rs soaktestrsbad
```

###### _output:_
```
# replicaset "soaktestrs" deleted
```

```bash
kubectl delete rs soaktestrs1
```

###### _output:_
```
# replicaset "soaktestrs1" deleted
```

```bash
kubectl delete rs soaktestrsbad
```

###### _output:_
```
# replicaset "soaktestrsbad" deleted
```

Again, the pods that were created are deleted when we delete the Replica Set.

```bash
kubectl get all -o wide
```

There shouldn't be any resource.

---

This concludes part 2 of Hands-on Exercise #2.  Continue on to Part 3 next.

[Part 3: Deployments](Part-03-Deployments.md)
