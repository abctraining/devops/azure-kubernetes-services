# Exercise #2 : Part 1
## ReplicaControllers

---

### Intro

As microservices are designed and implemented, sometimes mistakes are made with where boundaries are drawn between different microservices.  The solution is to merge or decompose the functionality of microservices.  In Kubernetes the 'ReplicaController' is one such example that was later decomposed functionally into two different microservices (ReplicaSets & Deployments).   The 'ReplicaController' still exists to provide continued functionality to older workloads on newer clusters.  Even though it is not recommended to continue to use ReplicaControllers we will explore in them in this exercise because it is important to understand them to help comprehend why there is ReplicaSets and Deployments.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/02-PodControllers01
~~~

### ReplicaControllers

Most of this structure should look familiar from our discussion of Deployments; we’ve got the name of the actual
Replication Controller (soaktestrc) and we’re designating that we should have 3 replicas, each of which are
defined by the template.  The selector defines how we know which pods belong to this Replication Controller.

Now tell Kubernetes to create the Replication Controller based on that file:

```bash
kubectl create -f rc.yaml
```

###### _output:_
```
replicationcontroller "soaktestrc" created
```

Let’s take a look at what we have using the describe command:

```bash
kubectl describe rc soaktestrc
```

###### _output:_
```
Name:           soaktestrc
Namespace:      default
Image(s):       nickchase/soaktest
Selector:       app=soaktestrc
Labels:         app=soaktestrc
Replicas:       3 current / 3 desired
Pods Status:    3 Running / 0 Waiting / 0 Succeeded / 0 Failed
No volumes.
Events:
  FirstSeen     LastSeen        Count   From                            SubobjectPath   Type   Reason                   Message
  ---------     --------        -----   ----                            -------------   --------------                  -------
  1m            1m              1       {replication-controller }                       Normal SuccessfulCreate Created pod: soaktestrc-g5snq
  1m            1m              1       {replication-controller }                       Normal SuccessfulCreate Created pod: soaktestrc-cws05
  1m            1m              1       {replication-controller }                       Normal SuccessfulCreate Created pod: soaktestrc-ro2bl
```

As we can see, we’ve got the Replication Controller, and there are 3 replicas (pods), of the 3 pods that we wanted.
All 3 of them are currently running.  You can also see the individual pods listed underneath, along with their names.  We added the --show-labels options and -l option for selecting by labels

```bash
kubectl get rc,po --show-labels -l app=soaktestrc
```

###### _output:_
```
NAME               READY     STATUS    RESTARTS   AGE
soaktestrc-cws05   1/1       Running   0          3m
soaktestrc-g5snq   1/1       Running   0          3m
soaktestrc-ro2bl   1/1       Running   0          3m
```

Delete one of the pods (out of 3)
If you delete one of the pods manually, then the replication controller will create one to match the desired numbers of pods to be 3.

```bash
kubectl delete pod/<pod_name>
```

###### _output:_
```
pod "<pod_name>" deleted
```

Check the pods Again
```bash
kubectl get pods -o wide
```
You will see three pods again

Clean up the Replication Controllers

```bash
kubectl delete rc soaktestrc
```

###### _output:_
```
replicationcontroller "soaktestrc" deleted
```

check if the pods got deleted

```bash
kubectl get rc,po --show-labels -l app=soaktestrc
```

###### _output:_
```
No resources found in default namespace.

```
As you can see, when you delete the Replication Controller, you also delete all of the pods that it created.

---


---

This concludes part 1 of Hands-on Exercise #2.  Continue on to Part 2 next.

[Part 2: ReplicaSets](Part-02-ReplicaSets.md)
