# Exercise #2 : Part 3
## Deployments

---

### Intro

The second of the two microservices resulting in the decomposition of the ReplicaController is the 'Deployment'.  The Deployment is responsible to manage the lifecycle of different ReplicaSets representing different versions of your application.  Deployments main responsibility is to handle all the rolling out and rolling back changes to each deployment of a microservice or application.  In Kubernetes 'Deployments' are the primary object used to run workloads.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/02-PodControllers01
~~~

### Deployments

Deployments are intended to replace Replication Controllers.  They provide the same replication functions
(through ReplicaSets) and also the ability to rollout changes and roll them back if necessary.

You first have to create a simple Deployment (you can find it in the git repo)  

`deploy_backed_by_rs.yaml`

Now go ahead and create the Deployment:

```bash
kubectl create -f deploy_backed_by_rs.yaml
```

###### _output:_
```
deployment.extensions/soaktest created
```

Now let’s go ahead and describe the Deployment:

```bash
kubectl describe deployment soaktest
```

###### _output:_
```
Name:                   soaktest
Namespace:              default
CreationTimestamp:      Sun, 05 Mar 2017 16:21:19 +0000
Labels:                 app=soaktest
Selector:               app=soaktest
Replicas:               5 updated | 5 total | 5 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  1 max unavailable, 1 max surge
OldReplicaSets:         <none>
NewReplicaSet:          soaktest-3914185155 (5/5 replicas created)
Events:
  FirstSeen     LastSeen        Count   From                            SubobjectPath   Type    Reason                   Message
  ---------     --------        -----   ----                            -------------   --------------                   -------
  38s           38s             1       {deployment-controller }                        Normal  ScalingReplicaSet        Scaled up replica set soaktest-3914185155 to 3
  36s           36s             1       {deployment-controller }                        Normal  ScalingReplicaSet        Scaled up replica set soaktest-3914185155 to 5
  ```

As you can see, rather than listing the individual pods, Kubernetes shows us the Replica Set.  
Notice that the name of the Replica Set is the Deployment name and a hash value.

We can check the pods for the deployment (and also behind the scenes the deployment creates a replica sets)


Check all deployments, pods, RCs, RSs

```bash
kubectl get deploy,po,rs,rc
```

###### _output:_
```
NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/soaktest   5/5     5            5           42s

NAME                            READY   STATUS    RESTARTS   AGE
pod/soaktest-5d78c4cfcc-875tn   1/1     Running   0          42s
pod/soaktest-5d78c4cfcc-lx8fw   1/1     Running   0          42s
pod/soaktest-5d78c4cfcc-n59wb   1/1     Running   0          42s
pod/soaktest-5d78c4cfcc-v7gkl   1/1     Running   0          42s
pod/soaktest-5d78c4cfcc-zxkrc   1/1     Running   0          42s

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/soaktest-5d78c4cfcc   5         5         5       42s
```

You can see that there is 1 deployment, 5 pods (because of 5 replication), 1 replica set BUT no replication controller.
This is because deployment created replica set but not replication controller

##### Cleanup

```bash
kubectl delete deployment.apps/soaktest
```

_This deletes Deployment, ReplicaSets and Pods_


---

This concludes part 3 of Hands-on Exercise #2.

Return to the [Exercise Page](../README.md)
