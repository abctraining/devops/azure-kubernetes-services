# Azure Kubernetes Services: The Essentials
# Hands-on Exercises #2

### Objective

In these exercises we will explore higher-level constructs that create and control Pods in Kubernetes.

### Parts

[Part 1: ReplicaControllers](Part-01-ReplicaControllers.md)

[Part 2: ReplicaSets](Part-02-ReplicaSets.md)

[Part 3: Deployments](Part-03-Deployments.md)

Return to the course [Table of Content](../README.md)
