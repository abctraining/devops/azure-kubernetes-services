# Azure Kubernetes Services - Labs

* [00 - Setup AKS Cluster](00-AKSSetup) - Azure - AKS - Cloud Shell - kubectl`
* [01 - Pods, Namespaces, Labels](01-ResourceGrouping) - Pods - Namespaces - Labels
* [02 - Pod Controllers01](02-PodControllers01) - ReplicaControllers - ReplicaSets - Deployments
* [03 - Networking](03-Networking) - Services - ClusterIP - NodePort - LoadBalancer - Ingress
* [04 - PodSpec Add-ons](04-PodSpec) - ConfigMaps - Secrets - Volumes - Resource Limits - InitContainers
* [05 - Pod Controllers02](05-PodControllers02) - HorizontalPodAutoscalers - DaemonSets - StatefulSet / PersistentVolumes
* [06 - Orchistration and Administration](06-AppControl) -  Discovery - Jobs / CronJobs - Logging
* [07 - Helm](07-Helm) - Helm

---

###### Lab Files

_Copy Lab repo local_
```bash
mkdir -p ~/content
cd ~/content
git clone https://gitlab.com/abctraining/devops/azure-kubernetes-services.git
cd azure-kubernetes-services
```
