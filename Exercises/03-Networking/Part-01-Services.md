# Exercise #3 : Part 1
## Services

---

### Intro

So far we have seen individual 'Pods' or groups of 'Pods' that are still made up of individual "Pods".  These pods represent Microservices or sometimes entire applications so they need to be connected to from clients both inside and outside the Kubernetes cluster.  The Pods come and go as there replica count or as there are changes to the PodSpec.  We treat 'Pods' as ephemeral but we need a method to connect across the network to these pods that is not going to change.  Kubernetes provides the 'Service' object to do just that.  Services provide a constant IP and port for clients to connect to that is loadbalenced between all running replicas of the microservice / application provided by the backend pods.  You can think of the "Service" as the front door of each "microservice" within your application deployment on Kubernetes.  In this Exercise we will explore the basics exposing pods to clients through a 'Service'.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/03-Networking
~~~

### Services

Create a pod, deployment and service using the Imperative method

Create a deployment nginx

```bash
kubectl create deployment nginx-deployment  --image nginx --port 80
```

Check the currently running pods, deployments, services and replica sets

```bash
kubectl get po,deploy,svc,rs -o wide
```

Expose the Deployment as a Service of Loadbalancer

```bash
kubectl expose deploy/nginx-deployment --type=LoadBalancer --name nginx-service
```

Check the currently running pods, deployments, services and replica sets

```bash
kubectl get po,deploy,svc,rs -o wide
```

_Check for the nginx application from url , using the IP_

Do a describe on the nginx service , using the IP try doing a curl for that port

```bash
kubectl describe svc nginx-service
```

verify by: `curl <ip_address>:<port_number>`

Other ways to verify

```bash
kubectl proxy &
curl http://localhost:8001/api/v1/namespaces/default/services/nginx-service/proxy/
curl http://$(kubectl get svc nginx-service --template='{{.spec.clusterIP}}'):80
```
---

Create a Service from a Pod

```bash
kubectl create -f sample_pod.yaml
```

Use `exec` and `logs` commands

```bash
IP=`kubectl describe pod/nginx-apparmor | grep -i "^IP:" | awk -F" " '{print $2}'`
curl $IP:80
kubectl logs pod/nginx-apparmor
```

You will see the logs

```bash
kubectl exec -it pod/nginx-apparmor -- /bin/bash
```

This will take you to the container, if pod has multiple containers, use -c option for container name.

Use `exit` to leave of the container.

Check the currently running pods

```bash
kubectl get po -o wide
```

Expose the pod as a service

```bash
kubectl expose pod nginx-apparmor --type NodePort --name nginx-apparmor-service
```

check running services again

```bash
 kubectl get po,deploy,svc,rs -o wide
```

###### verify

```bash
kubectl describe svc nginx-apparmor-service
```

verify by
```bash
curl <ip_address>:<port_number>
```

### Clean up

```bash
kubectl delete svc nginx-service
kubectl delete deploy nginx-deployment
kubectl delete svc nginx-apparmor-service
kubectl delete pod nginx-apparmor
```

To stop the proxy we must first bring the job to the foreground.

```bash
jobs
fg
```

Use `CTRL+C` to stop the proxy while it is in the foreground.


---

This concludes part 1 of Hands-on Exercise #3.  Continue on to Part 2 next.

[Part 2: ClusterIP](Part-02-ClusterIP.md)
