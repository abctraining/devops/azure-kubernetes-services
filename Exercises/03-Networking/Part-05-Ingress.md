# Exercise #3 : Part 5
## Ingress

---

### Intro

From a networking perspective Kubernetes Services are how we get traffic to a group of pods in a Kubernetes cluster.  Every Kubernetes Service requires a dedicated external IP address.  External IP addresses are many times finite resources that each have a cost associated.  In addition to that most of the connections today utilize a web services of some sort, HTTP or one of its derivatives.  Kubernetes provides an application level load-balancer in the form a proxy with the 'Ingress' object.  In this section we will explore Kubernetes Ingress resources and IngressControllers that facilitate those Ingress resources.    

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/azure-kubernetes-services/src/03-Networking
~~~
### Ingress

#### YAML files

In that directory you will find the following manifest files for these Ingress exercises.

test-ingress.yaml

~~~yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: test-ingress
spec:
  defaultBackend:
    service:
      name: test
      port:
        number: 80
~~~

simple-fanout-example.yaml

~~~yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: simple-fanout-example
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: foo.bar.com
    http:
      paths:
      - path: /foo
        pathType: Prefix
        backend:
          service:
            name: service1
            port:
              number: 80
      - path: /bar
        pathType: Prefix
        backend:
          service:
            name: service2
            port:
              number: 80
~~~

name-virtual-host-ingress.yaml

~~~yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: name-virtual-host-ingress
spec:
  rules:
  - host: foo.bar.com
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: service1
            port:
              number: 80
  - host: bar.foo.com
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: service2
            port:
              number: 80
~~~

### Ingress

Let us see what we are starting with

~~~bash
kubectl get all,cm,pv,pvc,ingress,ingressclass -A
~~~

##### Install ingress-nginx

If you have not yet installed the ingress controller let us do install ingress-nginx ingress controller now.

~~~bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.0/deploy/static/provider/cloud/deploy.yaml
~~~

See what was installed with ingress-nginx

~~~bash
kubectl get all,cm,pv,pvc,ingress,ingressclass -n ingress-nginx
~~~

Create a deployment to use with Ingress

~~~bash
kubectl create deployment test --image=nginx:alpine --port=80
kubectl expose deploy/test
~~~

Apply the ingress from test-ingress.yaml

~~~bash
kubectl apply -f test-ingress.yaml
~~~

View the newly created ingress

~~~bash
kubectl get ingress
~~~

Connect to site (replace the IP with the Ingress controllers external IP)

~~~bash
curl http://{Service-EXTERNAL-IP}
~~~

##### Clean-up

~~~bash
kubectl delete ingress/test-ingress
kubectl get ingress
~~~

### Fan out and Named Based split

Create additional services to connect to

~~~bash
kubectl create deployment service1 --image=httpd:alpine --port=80
kubectl create deployment service2 --image=caddy:alpine --port=80
kubectl expose deploy/service1
kubectl expose deploy/service2
~~~

Create the index

~~~bash
kubectl apply -f simple-fanout-example.yaml
kubectl get ingress
~~~

This is one of the tricker parts of the exercise as we need to get `foo.bar.com` and `bar.foo.com` to resolve to the Services EXTERNAL-IP.  There are a number of ways to do that but the simplest many times is to add a line to your system `hosts` file.  This is done differently on different type type of systems and is beyond the scope of this lab.  I did include a few links that may be able to help modify your system "Hosts" file.  If we can't complete this we may have to skip a few of these Ingress connections from the outside.  In normal situations we would update public DNS records appropriately to resolve our hostname to the service external IP address.

Set foo.bar.com & bar.foo.com to LoadBalancer External IP.  Here are a few sites that may help:

[https://linuxize.com/post/how-to-edit-your-hosts-file/](https://linuxize.com/post/how-to-edit-your-hosts-file/)

[https://www.liquidweb.com/kb/dns-hosts-file/](https://www.liquidweb.com/kb/dns-hosts-file/)

If you are able to come up with a solution try connocting to these two URLs to see the Ingress in action.

~~~bash
http://foo.bar.com/foo
http://foo.bar.com/bar
~~~


##### Clean-up

~~~bash
kubectl delete -f simple-fanout-example.yaml
~~~

### Name Based

Install ingress resource

~~~bash
kubectl apply -f name-virtual-host-ingress.yaml
~~~

View the resource

~~~bash
kubectl get ingress
~~~

Connect to the sites (Same issue with resolving as before, skip if needed)

~~~bash
curl http://foo.bar.com
curl http://bar.foo.com
~~~

##### Clean-up

Remove deployments

~~~bash
kubectl delete deploy/test
kubectl delete deploy/service1
kubectl delete deploy/service2
~~~

Remove sevices

~~~bash
kubectl delete svc/test
kubectl delete svc/service1
kubectl delete svc/service2
~~~

It is not needed but if you want to also remove the nginx-ingress controller run this following command:

~~~bash
kubectl delete -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.0/deploy/static/provider/cloud/deploy.yaml
~~~

---

This concludes part 5 of Hands-on Exercise #3.

Return to the [Exercise Page](../README.md)
